<?php

// Clasa users repository face legatura dintre controller si baza de date si returneaza sau primeste obiecte instantiate prin model
// Pentru ca toate metodele clasei usersrepository au nevoie de o legatura cu baza de date, ca si in cazul controllerului o sa facem o proprietate privata numita $database si o sa o initializam in construct_ul usersrepositorului 

Class UsersRepository  {

	private $database;

	public function __construct(){
		$this->database = new Database(); // Database este o clasa care se afla in Helpers si care nu o sa o scriu acum dar care clasa respectiva contine in metoda ei construct o conectare la db, in destruct contine un mysql_close si o metoda execute care foloseste mysqli_query si pe care query il dam ca si parametru in functia execute 

		// ne intoarcem la fisierul userscontroller >>

		

	} 
	
	// ne intoarcem din fisierul users controller
	public function list(){
		// facem un query in MySQL pentru a selecta tot din db pentru tabela users
		$query = "SELECT * FROM users";
		// acest query trebuie sa il executam pe db si facem asta returnand un rezultat in variabila $results dupa ce am apelat query-ul pe db
		$results = $this->database->execute($query); // metoda execute din Database o sa ne returneze un mysqli_resource care contine toate intrarile din db si o sa bagam toate datele astea in variabila $results

		// pentru ca variabila results este un mysqli_resource, trebuie sa facem o bucla pentru a scoate datele pe rand. Prin utilizarea functiei mysqli_fetch_array o sa primim din mysqli_result un array ce contine o linie din baza de date. ceea ce se intampla cand facem mysqli_fetch_array este ca, daca avem 5 rezultate in result, atunci o sa ne returneze prin apelarea functiei primul rand in format array si o sa ramana in variabila ce contine resursa, doar restul de 4. Deci daca apelam inca odata mysqli_fetch_Array pe aceeasi variabila, o sa ne dea urmatorul rand.
		// ca sa facem chestia asta pana ramanem fara chestii in mysql_resutl, folosim o bucla care se executa cat timp returnam un rezultat != null
		while ($item = mysqli_fetch_array($results,MYSQLI_ASSOC)) {
			// folosim constanta mysqli_assoc pentru ca vrem sa primim in $item un array doar cu cheile array-ului si nu cu cheile avand cifre (vezi descrierea functiei mysqli_fetch_array pe php.net)
			// pentru ca am stabilit ca repository-ul returneaza obiecte (pentru functiile care returneaza rezultate si nu flag-uri - flagurile sunt true si false pentru ca daca facem un insert sau un update sau un delete, da pentru selecturi returnam date, deci obiecte)
			// atunci facem o variabila $user care o sa fie o instanta a clasei model user si care o sa contina ca si proprietati toate fieldurile din db de la tabela user. 

			$array = array();

			$user = new User($item["id"],$item["name"],$item["email"],$item["password"]); // user este clasa model a user-ului care contine proprietatile si constructia obiectului in metoda construct. in parantezele de la clasa user se parametrii clasei construct
			// mergem la models/users.php >>

			// ne intoarcem din users.php

			// dupa ce am facut obiectul, o sa bagam obiectul intr-un array pe care l-am declarat inainte de while  
			array_push($array,$user);
		}
		// dupa ce am iesit din bucla avem un array cu obiectele chestiilor din db si atunci dam return la arrayul ala
		return $array;
	}




}