<?php

// se defineste clasa DefaultController care prin definitie am stabilit ca este un copil al clasei Controller. De ce? pentru ca vreau clasa controller sa fie o clasa abstracta care contine o metoda abstracta defaultAction. In fisierul index, daca nu este definita o actiune, se va apela by default actiunea defaultAction. Din acest motiv, trebuie sa ne asiguram ca fiecare clasa controller contine o metoda numita defaultAction. Si cum putem face asta? Prin definirea unei metode abstracte intr-o clasa abstracta. Astfel, toate clasele copil ale clasei abstracte o sa fie muszai sa aiba defaultAction.

// Mergem in clasa Controller

// revenim din clasa controller

Class DefaultController extends Controller {

	// prima chestie pe care o facem in clasa defaultcontroller este functia defaultAction

	public function defaultAction(){

		// aici scriem cod php si dupa ce terminam includem fisierul de view al metodei respective care o sa se afle in folderul Views slash folderul numelui controllerului si anume Default in fisierul numelui metodei si anume default.php
		require "Views/Default/default.php"; // treba voastra e sa faceti un fisier pe calea asta si sa scrieti ceva frumos in el cum ar fi ... bine ai venit pe site

		//revenim la fisierul index.php >>
	}


}