<?php
//primul lucru pe care il vom face in controllerul asta o sa fie sa facem functia defaultAction, pentru ca daca nu o facem o sa ne dea o eroare pentru ca am stabilit ca userscontroller este un copil al clasei abstracte controller ce contine metoda abstracta defaultAction


Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
		// trebuie sa facem clasa UsersRepository care prin constructie ne-am hotarat ca o bagam in folderul Models/Repositories
	}

	public function defaultAction(){
		// aici o sa scriem codul pe care vrem sa il vedem cand apelam index.php?C=Users sau index.php?C=Users&A=default
	}

	public function listAction(){
		// list action ne va afisa toti utilizatorii. pentru a afisa toti utilizatorii, trebuie sa apelam repository-ul utilizatorilor si sa primim un array de obiecte de tip users. Din acest motiv trebuie sa facem repositoryul usersRepository si modelul usekrs

		// deoarece clasa UsersRepository o sa fie folosita in majoritatea actiunilor din controller, ar fi bine sa o declaram o singura data. din acest motiv, o sa facem o proprietate a clasei controller numita $usersRepository pe care o sa o initializam cu clasa UsersRepository in construct-ul clasei usersController. proprietatea o sa fie total privata pentru ca vrem sa o folosim doar in aceasta clasa.

		// ne-am intors din usersrepository

		$users = $this->usersRepository->list(); // list este o metoda a clasei usersRepository care returneaza toate rezultatele din db pentru tabela respectiva

		// mergem in usersrepository la metoda list
		// ne intoarcem din metoda list si constatam ca avem un array de obiecte in $users

		// pentru ca o variabila definita intr-o functie/metoda are ca si scop functia in care este definita si fisierele incluse in acea functie, pentru ca vom include fisierul de view al Views/Users/list, variabila $users o sa fie existenta si in fisierul de views

		// motiv pentru care in fisierul views/users/list o sa pot face un foreach pentru a afisa utilizatorii

		
	}

}