<?php

//UPDATE dupa ce am facut fisierul autoload
include ("Helpers/Autoload.php");

//1.
// MVC prin initializare va instantia un controller si va apela o metoda
// Daca exista date in link variabile $_GET pentru controller si actiune, atunci facem instanta.

// $_GET este un array care se instantiaza pentru fiecare fisier .php 
// daca nu exista nici o variabila in link care sa fie preluata cu $_GET, aceasta variabila numita $_GET o sa fie totusi definita dar goala (o sa aiba 0 elemente), motiv pentru care nu trebuie sa verificam daca exita $_GET ci doar daca $_GET contine ceea ce ne intereseaza
if (array_key_exists("C",$_GET)){ // aici se verifica daca variabila $_GET contine cheia C care pentru mine este setata ca si variabila numelui controllerului
	$controller = $_GET["C"]."Controller"; // daca exista voi lua continutul variabilei C care va contine defapt numele modelului si atunci voi face un append cu "Controller" pentru a declara numele clasei controller atasata modelului
} else {
	$controller = "DefaultController"; // daca nu exista $_GET[C] voi initializa controllerul DefaultController care prin constructie reprezinta controllerul care se apeleaza daca nu exista nimic setat in link
}

// acelasi lucru pe care l-am facut pentru controller il facem si pentru actiune
if (array_key_exists("A",$_GET)){
	$action = $_GET["A"]."Action"; //prin constructie, toate metodele controllerului incep cu litera mica si au la sfarsitul numelui metodei cuvantul Action
} else {
	$action = "defaultAction";
}

$object = new $controller(); // declararea unei clase se poate face utilizand numele ei sau utilizand o variabila string ce contine numele clasei. In acest caz, numele clasei se afla in variabila controller, si astfel, daca dam new de $controller se va initializa un obiect in variabila $object conform definitiei clasei ale carui nume se afla in variabila $controller
$object->$action(); // apelarea unei metode se poate face utilizand numele metodei sau utilizand o variabila string ce contine numele acestei metode.

// in incercarea de a instantia o clasa controller, vom avea o eroare deoarece clasa controllerului nu se afla in acest fisier. din acest motiv ar trebui sa includem fisierul ce contine aceasta clasa. Functia __autoload face acest lucru, motiv pentru care vom scrie acum functia autoload din fisierul helpers/autoload.php.
// helpers este folderul in care se adauga clasele si functiile aditionale MVC-ului cum ar fi autoload, legatura la DB, fisierul de log, etc ...

// mergeti in fisierul autoload >>

//Continuare
// pentru ca nu avem nici un controller definit in link by default, o sa facem fisierul DefaultController

// mergeti in fisierul DefaultController >>

// am revenit din fisierul defaultController

// daca de exemplu avem setat numele controllerului si numele actiunii in link, cum ar fi C=Users&A=list, atunci se va apela controllerul UsersController si actiunea listAction, motiv pentru care vom face fisierul UsersController

// mergem in fisierul UsersController >>

