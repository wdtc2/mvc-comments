<?php
// autolaod este o functie care se apeleaza automat de catre codul php cand incercam sa instantiem un obiect dintr-o clasa si nu gasim clasa definita. astfel pentru a defini clasa, putem sa luam numele clasei si sa incercam printr-o regula de notatie sa instantiem clasa

function __autoload($class){ // functia __autoload este o functie magica si trebuie definita cu un parametru obligatoriu $class (nu trebuie muszai sa se numeasca class dar ar fi bine) care va reprezenta numele clasei care va fi instantiata si care nu va fi gasita de catre codul php.
	// in aceasta functie autoload o sa definim un array care o sa contina toate caile posibile in case se gasesc clase
	$folders = array(
		"Controllers",
		"Models"
	);
	// pentru fiecare element al arrayului $folders, trebuie sa facem urmatoarele verificari
	foreach ($folders as $folder){
		// variabila folder va contine numele folderului si atunci trebuie sa facem intr-o variabila calea absoluta a fisierului posibil de inclus
		$filepath = $folder."/".$class.".php"; // numele folderul, slash, numele clasei si extensia .php - prin definitie am stabilit ca numele fisierului ce contine o clasa va avea exact numele clasei
		if (file_exists($filepath)){ // verificam daca exista fisierul pe disc la calea generata de noi
			require_once $filepath; // daca exista fisierul, vom face require_once, si nu require pentru ca o clasa trebuie sa fie definita doar o singura data. 
		}
	}
}

// avand in vedere ca functia autoload se afla in fisierul acesta, trebuie sa facem include de helpers/autoload.php in prima parte a fisierului index.php

// revenim la index.php